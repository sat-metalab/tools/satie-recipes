//Boot SATIE
(
~config = SatieConfiguration(
        listeningFormat: [\stereoListener],
        outBusIndex: [0],
        userPluginsDir: PathName.new(thisProcess.nowExecutingPath.dirname).parentPath+/+"plugins"
);
~satie = Satie(~config);
~satie.boot();
s.plotTree;
s.meter;
s.makeGui;
)


//Register the SynthDef to use it with Pbind
SynthDescLib.global.add(~satie.synthDescLib.at(\FMSineX));


//This SynthDef can produce interesting sound effects like old school lazers, UFOs or sci-fi sounds...


---------------
//Slow sequence
(
p = Pbind(
	\instrument, \FMSineX,
	\dur, 2/1,
	\group, ~satie.groups[\default].asNodeID,
	\note, Prand([1, 50], inf), \atk, Pexprand(0.01, 1), \dec, 3, \sus, 1, \rel, 0.5, \amp, 0.5, \gainDB, 0.2, \aziDeg, Pfunc {(1.0.rand2 - 0.5) * 180},
	\lfoHz1Speed, Pexprand(0.01, 100), \hz1Min, Pexprand(20, 500), \hz1Max, Pexprand(500, 10000), \lfoAmp1Speed, Pexprand(0.01, 100), \amp1Min, Pwhite(0.01, 100), \amp1Max, Pexprand(500, 10000), \mod1amp, Pwhite(0.01, 1),
	\lfoHz2Speed, Pexprand(0.01, 100), \hz2Min, Pexprand(20, 500), \hz2Max, Pexprand(500, 10000), \lfoAmp2Speed, Pexprand(0.01, 100), \amp2Min, Pwhite(0.01, 100), \amp2Max, Pexprand(500, 10000), \mod2amp, Pwhite(0.01, 1),
	\lfoHz3Speed, Pexprand(0.01, 100), \hz3Min, Pexprand(20, 500), \hz3Max, Pexprand(500, 10000), \lfoAmp3Speed, Pexprand(0.01, 100), \amp3Min, Pwhite(0.01, 100), \amp3Max, Pexprand(500, 10000), \mod3amp, Pwhite(0.01, 1)
).play;
)

p.stop;


---------------
//Fast sequence
(
e = Pbind(
	\instrument, \FMSineX,
	\dur, 1/4,
	\group, ~satie.groups[\default].asNodeID,
	\note, Prand([1, 50], inf), \atk, Pexprand(0.01, 0.5), \dec, 1, \sus, 0.2, \rel, 0.2, \amp, 0.5, \gainDB, 0.2, \aziDeg, Pfunc {(1.0.rand2 - 0.5) * 180},
	\lfoHz1Speed, Pexprand(0.01, 100), \hz1Min, Pexprand(20, 500), \hz1Max, Pexprand(500, 10000), \lfoAmp1Speed, Pexprand(0.01, 100), \amp1Min, Pwhite(0.01, 100), \amp1Max, Pexprand(500, 10000), \mod1amp, Pwhite(0.01, 1),
	\lfoHz2Speed, Pexprand(0.01, 100), \hz2Min, Pexprand(20, 500), \hz2Max, Pexprand(500, 10000), \lfoAmp2Speed, Pexprand(0.01, 100), \amp2Min, Pwhite(0.01, 100), \amp2Max, Pexprand(500, 10000), \mod2amp, Pwhite(0.01, 1),
	\lfoHz3Speed, Pexprand(0.01, 100), \hz3Min, Pexprand(20, 500), \hz3Max, Pexprand(500, 10000), \lfoAmp3Speed, Pexprand(0.01, 100), \amp3Min, Pwhite(0.01, 100), \amp3Max, Pexprand(500, 10000), \mod3amp, Pwhite(0.01, 1)
).play;
)

e.stop;


---------------
//Even faster
(
f = Pbind(
	\instrument, \FMSineX,
	\dur, 1/8,
	\group, ~satie.groups[\default].asNodeID,
	\note, Prand([1, 50], inf), \atk, Pexprand(0.01, 0.05), \dec, 0.2, \sus, 0.05, \rel, 0.1, \amp, 0.5, \gainDB, 0.2, \aziDeg, Pfunc {(1.0.rand2 - 0.5) * 180},
	\lfoHz1Speed, Pexprand(0.01, 100), \hz1Min, Pexprand(20, 500), \hz1Max, Pexprand(500, 10000), \lfoAmp1Speed, Pexprand(0.01, 100), \amp1Min, Pwhite(0.01, 100), \amp1Max, Pexprand(500, 10000), \mod1amp, Pwhite(0.01, 1),
	\lfoHz2Speed, Pexprand(0.01, 100), \hz2Min, Pexprand(20, 500), \hz2Max, Pexprand(500, 10000), \lfoAmp2Speed, Pexprand(0.01, 100), \amp2Min, Pwhite(0.01, 100), \amp2Max, Pexprand(500, 10000), \mod2amp, Pwhite(0.01, 1),
	\lfoHz3Speed, Pexprand(0.01, 100), \hz3Min, Pexprand(20, 500), \hz3Max, Pexprand(500, 10000), \lfoAmp3Speed, Pexprand(0.01, 100), \amp3Min, Pwhite(0.01, 100), \amp3Max, Pexprand(500, 10000), \mod3amp, Pwhite(0.01, 1)
).play;
)

f.stop;


---------------
//Slightly different sound, rotating around the listener
(
g = Pbind(
	\instrument, \FMSineX,
	\dur, 1/8,
	\group, ~satie.groups[\default].asNodeID,
	\note, Prand([10, 70], inf), \atk, 0.001, \dec, 0.1, \sus, 0, \rel, 0.5, \amp, 0.5, \gainDB, 0.2, \aziDeg, Pseq([-180, -160, -140, -120, -100, -80, -60, -40, -20, 0, 20, 40, 60, 80, 100, 120, 140, 160], inf, 10),
	\lfoHz1Speed, Pexprand(0.01, 0.5), \hz1Min, Pexprand(5, 50), \hz1Max, Pexprand(50, 500), \lfoAmp1Speed, Pexprand(0.01, 50), \amp1Min, Pwhite(10, 50), \amp1Max, Pexprand(50, 300), \mod1amp, Pwhite(30, 50),
	\lfoHz2Speed, Pexprand(0.01, 0.5), \hz2Min, Pexprand(5, 50), \hz2Max, Pexprand(50, 500), \lfoAmp2Speed, Pexprand(0.01, 50), \amp2Min, Pwhite(10, 50), \amp2Max, Pexprand(50, 300), \mod2amp, Pwhite(30, 50),
	\lfoHz3Speed, Pexprand(0.01, 0.5), \hz3Min, Pexprand(5, 50), \hz3Max, Pexprand(50, 500), \lfoAmp3Speed, Pexprand(0.01, 50), \amp3Min, Pwhite(10, 50), \amp3Max, Pexprand(50, 300), \mod3amp, Pwhite(30, 50)
).play;
)

g.stop;