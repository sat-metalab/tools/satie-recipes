## Dependencies

##### Some SATIE-Recipes presented in this repository make use of external UGens. These must be downloaded for these recipes to work properly.

The [Greyhole_examples](/configurations/Greyhole_examples.scd) recipe make use of external UGens :  


- JPverb is a reverb and Greyhole is an echo/delay, both are part of the [DEIND](https://doc.sccode.org/Overviews/DEIND.html) project. They are available in the latest release of the [SC3-Plugins](http://supercollider.github.io/sc3-plugins/).

- The SC3-Plugins folder must be placed in the Extensions folder of SuperCollider.  

- To load these UGens with SATIE, please also download their SATIE version here : [Gravity](/plugins/sources/Gravity.scd), [Greyhole](/plugins/effects/Greyhole.scd), [GreyholeChaos](/plugins/effects/GreyholeChaos.scd)