// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//Every sound is made from sine waves

~name = \FMSine;
~description = "Frequency Modulation using sine waves";
~channelLayout = \mono;

~function = {

	|amp = 1, note = 69, atk = 0.1, dec = 3, sus = 0, rel = 2, gate = 1,
	mod1Freq = 440, mod1Amp = 1, mod1gain = 1,
	mod2Freq = 440, mod2Amp = 1, mod2gain = 1,
	mod3Freq = 440, mod3Amp = 1, mod3gain = 1|

	var sig, env, car, mod1, mod2, mod3;

	env = EnvGen.kr(Env.adsr(atk, dec, sus, rel), gate, doneAction:2);

	mod1 = SinOsc.ar(
		freq: mod1Freq,
	    mul: mod1Amp
	) * mod1gain;
    mod2 = SinOsc.ar(
	    freq: mod2Freq,
	    mul: mod2Amp
    ) * mod2gain;
    mod3 = SinOsc.ar(
	    freq: mod3Freq,
	    mul: mod3Amp
    ) * mod3gain;

	car = SinOsc.ar(note.midicps + mod1 + mod2 + mod3) * env * amp;

};